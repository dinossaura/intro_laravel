<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/single/{slug}', 'HomeController@single')->name('product.single');

Route::get('/category/{slug}','ViewProductCategoryController@index')->name('category.single');
Route::get('/store/{slug}','ViewProductStoreController@index')->name('store.single');

Route::prefix('cart')->name('cart.')->group(function(){

    Route::get('/', 'CartController@index')->name('index');
    Route::post('add', 'CartController@add')->name('add');
    Route::get('remove/{slug}', 'CartController@remove')->name('remove');
    Route::get('cancel', 'CartController@cancel')->name('cancel');

});

Route::prefix('checkout')->name('checkout.')->group(function(){
    Route::get('/','CheckoutController@index')->name('index');
    Route::post('/processCheckout', 'CheckoutController@processCheckout')->name('processCheckout');
    Route::get('/thanks', 'CheckoutController@thanks')->name('thanks');
});

Route::group(['middleware' => ['auth']], function () {

    Route::get('my-orders', 'UserOrderController@index')->name('user.orders');

    Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function () {

      Route::get('notifications', 'NotificationController@notifications')->name('notifications.index');
      Route::get('notifications/read-all', 'NotificationController@readAll')->name('notifications.read.all');  
      Route::get('notifications/read/{id}', 'NotificationController@read')->name('notifications.read');  
  

//    Route::prefix('stores')->name('stores.')->group(function(){
//
//        Route::get('/', 'StoreController@index')->name('index');
//
//        Route::get('/create', 'StoreController@create')->name('create');
//
//        Route::post('/store', 'StoreController@store')->name('store');
//
//        Route::get('/{store}/edit', 'StoreController@edit')->name('edit');
//
//        Route::post('/update/{store}', 'StoreController@update')->name('update');
//
//        Route::get('/destroy/{store}', 'StoreController@destroy')->name('destroy');
//
//    });


        Route::resource('categories', 'CategoryController');

        Route::resource('stores', 'StoreController');

        Route::resource('products', 'ProductController');

        Route::post('photos/remove', 'ProductPhotoController@removePhoto')->name('photo.remove');

        Route::get('orders/my', 'OrdersController@index')->name('orders.my');

    });

});

Auth::routes();

