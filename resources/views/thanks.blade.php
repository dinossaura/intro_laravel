@extends('layouts.front')

@section('content')

    <h1 class="alert success">
        Muito obrigado pela sua compra!
    </h1>
    <h2 class="alert success">
        Seu pedido foi processado, código do pedido: {{request()->get('order')}}
    </h2>

@endsection