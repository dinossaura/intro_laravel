@extends('layouts.front')

@section('content')

    <div class="row front">
        <div class="col-12">
            <h2>{{$store->name}}</h2>
            <p>{{$store->description}}</p>
            <p>
                <strong>Contatos:</strong>
            <span>{{$store->phone}}</span> | <span>{{$store->mobile_phone}}</span>
            </p>
        </div>
        <div class="col-12">
            <h2>Produtos desta loja:</h2>
        </div>
        @forelse($store->products as $key => $product)
                <div class="col-md-4">
                    <div class="card">
                        @if ($product->photos->count())
                            <img style="width: 98%" src="{{asset('storage/'.$product->photos->first()->image)}}"
                                class="img-fluid" alt="">
                        @else
                            <img style="width: 98%" src="{{asset('assets/img/no-photo.jpg')}}"
                                class="img-fluid" alt="">
                        @endif
                        <div class="card-body">
                            <h2 class="card-title">{{$product->name}}</h2>
                            <p class="card-text">{{$product->description}}</p>
                            <h3>R$ {{number_format($product->price, '2', ',', '.')}}</h3>
                            <a href="{{route('product.single', ['slug' =>$product->slug])}}" class="btn btn-success">Ver Produto</a>

                        </div>
                    </div>
                </div>
                @if (($key + 1) % 3 == 0)
                    </div><div class="row front">
                @endif
        @empty
            <h3 class="alert alert-warning">
                Nenhum produto encontrado para essa loja.
            </h3>
        @endforelse  
    </div>
@endsection
