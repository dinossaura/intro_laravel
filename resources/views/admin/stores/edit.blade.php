@extends('layouts.app')

@section('content')
    <h1 class="mt-2">Editar loja : {{$store->name}}</h1>

    <form action="{{ route('admin.stores.update', ['store' => $store->id])  }}" method="post"
          enctype="multipart/form-data">
        @csrf
        @method("PUT")
        <div class="form-group">
            <label>Nome</label>
            <input class="form-control" type="text" name="name" value="{{$store->name}}">
        </div>

        <div class="form-group">
            <label>Descrição</label>
            <input class="form-control" type="text" name="description" value="{{$store->description}}">
        </div>

        <div class="form-group">
            <label>Telefone</label>
            <input class="form-control" type="text" name="phone" value="{{$store->phone}}">
        </div>

        <div class="form-group">
            <label>Celular</label>
            <input class="form-control" type="text" name="mobile_phone" value="{{$store->mobile_phone}}">
        </div>


        <div class="form-group">
            <label>Slug</label>
            <input class="form-control" type="text" name="slug" value="{{$store->slug}}">
        </div>

        <div class="form-group">
            <label for="">Logo</label>
            <input type="file" name="logo" class="form-control @error('logo') is-invalid @enderror ">
            @error('logo')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-success">Atualizar Loja</button>
        </div>

    </form>

    <hr>

    <div class="row">
            <div class="col-4 text-center">
                <img src="{{ asset('storage/logo/'.$store->logo)  }}" alt="" class="img-fluid">
            </div>
    </div>
@endsection
