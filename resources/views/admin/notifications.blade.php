@extends('layouts.app')

@section('content')
    <h1 class="mt-2">Lista de Produtos</h1>
<a href="{{ route('admin.notifications.read.all')}}" class="btn btn-sm btn-success mt-1 mb-1">Marcar todas como lidas</a>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Notificação</th>
            <th>Criado em</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        @forelse($unreadNotifications as $notification)
            <tr>
                <td>{{ $notification->data['message'] }}</td>
                <td>{{ $notification->created_at->format('d/m/Y H:i') }}  - {{ $notification->created_at->locale('pt')->diffForHumans()  }}</td>
                <td>
                    <div class="btn-group">
                    <a href="{{route('admin.notifications.read', ['id' => $notification->id])}}" class="btn btn-sm btn-primary">Marcar como lida</a>
                    </div>
                </td>
            </tr>

        @empty
            <tr>
                <td colspan="3">
                    <div class="alert alert-warning">Nenhuma notificação encontrada</div>
                </td>
            </tr>    
        @endforelse
        </tbody>
    </table>
@endsection
