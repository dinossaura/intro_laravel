<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model
{

    protected $fillable = ['reference', 'pagseguro_code', 'pagseguro_status', 'user_id', 'store_id', 'items'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function stores()
    {
        //pedidos possui uma ou mais lojas
        //passando como parametro a tabela da relaçao 
        return $this->belongsToMany(Store::class, 'order_store', 'order_id');
    }
}
