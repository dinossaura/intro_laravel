<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5 ',
            'description' => 'required|min:5',
            'phone' => 'required|min:5',
            'mobile_phone' => 'required|min:5' ,
            'logo' => 'image',
        ];
    }

    public function messages()
    {
        return [
            'min' => 'Campo deve ter no mínimo :min caracteres.',
            'required' => 'Este campo é obrigatório.',
            'image' => 'Arquivo não é uma imagem válida',
        ];
    }
}
