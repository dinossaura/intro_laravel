<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $cart = session()->has('cart') ? session()->get('cart') : [];

        return view('cart', [
            'cart' => $cart,
        ]);
    }

    public function add(Request $request)
    {
        $productData = $request->get('product');

        //info do produto no banco
        $product = Product::whereSlug($productData['slug']);

        //checando slug e qtde do produto
        if(!$product->count() || $productData['amount'] == 0){
            return redirect()->route('home');
        } 

        //passando na segunda chave as infos do produto que estao no banco
        //dessa forma não permite que haja aletracoes manuais nos inputs no front
        $product = array_merge($productData, $product->first(['name', 'price', 'store_id'])->toArray());
    
        if (session()->has('cart')) {

            $products = session()->get('cart');

            $productsSlugs = array_column($products, 'slug');

            //se na sesssão eu já tenho aquele slug que estou mandando novamente
            if (in_array($product['slug'], $productsSlugs)) {

                $products = $this->productIncrement($product['slug'], $product['amount'], $products);

                session()->put('cart', $products);

            } else {

                //criar array e passa o array para sessão
                $products[] = $product;
                session()->put('cart', $products);

            }

        } else {

            //criar array e passa o array para sessão
            $products[] = $product;
            session()->put('cart', $products);
        }


        flash('Produto adicionado no carrinho.')->success();

        return redirect()->route('product.single', [
            'slug' => $product['slug'],
        ]);

    }

    public function remove($slug)
    {
        if (!session()->has('cart')) {
            return redirect()->route('cart.index');

        }

        $products = session()->get('cart');
        //retorna todos os produtos menos o que foi passado no request para remoção
        $products = array_filter($products, function ($line) use ($slug) {
            return $line['slug'] != $slug;
        });


        session()->put('cart', $products);


        return redirect()->route('cart.index');

    }

    public function cancel()
    {
        session()->forget('cart');

        flash('Compra cancelada com sucesso!')->success();

        return redirect()->route('cart.index');
    }

    private function productIncrement($slug, $amount, $products)
    {
        $products = array_map(function ($line) use ($slug, $products, $amount) {
            if ($slug == $line['slug']) {
                $line['amount'] += $amount;
            }

            return $line;
        }, $products);

        return $products;
    }
}
