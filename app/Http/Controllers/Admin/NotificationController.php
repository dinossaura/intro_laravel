<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function notifications()
    {

        $unreadNotifications = auth()->user()->unreadNotifications;

        return view('admin.notifications', [
            'unreadNotifications' => $unreadNotifications,
        ]); 
    }

    public function readAll()
    {
        $unreadNotifications = auth()->user()->unreadNotifications;

        $unreadNotifications->each(function($notification){
            $notification->markAsRead();
        });

        flash('Notificações lidas com sucesso.');

        return redirect()->back();

    }

    public function read($id)
    {
        $notification = auth()->user()->notifications()->find($id);

        $notification->markAsRead();

        flash('Notificação marcada como lida.');

        return redirect()->back();

        
    }
}
