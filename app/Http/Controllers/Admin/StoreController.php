<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRequest;
use App\Store;
use App\Traits\UploadTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StoreController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('user.has.store')->only(['create', 'store']);
    }

    public function index()
    {
        $store = auth()->user()->store;

        return view('admin.stores.index', [
            'store' => $store,
        ]);
    }

    public function create()
    {
        return view('admin.stores.create');
    }

    public function store(StoreRequest $request)
    {

        $data = $request->all();

        $user = auth()->user();

        if ($request->hasFile('logo')) {

            $data['logo'] = $this->imageUpload($request->file('logo'));
        }

        $store = $user->store()->create($data);

        flash('Loja criada com sucesso')->success();


        return redirect()->route('admin.stores.index');
    }

    public function edit($store)
    {
        $store = Store::find($store);

        return view('admin.stores.edit', [
            'store' => $store,
        ]);
    }

    public function update(StoreRequest $request, $store)
    {
        $store = Store::find($store);
        $data = $request->all();


        if ($request->hasFile('logo')) {

            if (Storage::disk('public')->exists($store->logo)) {
                Storage::disk('public')->delete($store->logo);
            }

            $data['logo'] = $this->imageUpload($request->file('logo'));
        }


        $store->update($data);

        flash('Loja atualizada com sucesso')->success();

        return redirect()->route('admin.stores.edit', [
            'store' => $store,
        ]);
    }

    public function destroy($store)
    {
        $store = Store::find($store);

        if ($store->products()->count()) {

            flash('Existem produtos aninhados a essa loja por isso não é possível excluir.')->warning();

            return redirect()->route('admin.categories.index');
        }

        $store->delete();

        flash('Loja removida com sucesso')->success();

        return redirect()->route('admin.stores.index');
    }
}
