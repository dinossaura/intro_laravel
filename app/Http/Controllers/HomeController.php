<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Store;
use App\UserOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::limit(6)->orderBy('id', 'DESC')->get();

        $stores = Store::all();

        $list = DB::table('user_orders')
            ->select(DB::raw('count(user_orders.store_id) as quantity , stores.id as store'))
            ->join('stores', 'stores.id', '=', 'user_orders.store_id')
            ->groupBy('user_orders.store_id')
            ->orderBy('quantity', 'DESC')
            ->limit(3)
            ->get();


        $topStores = array_map(function ($item) use ($stores) {
            foreach ($stores as $store) {
                if ($store->id == $item->store) {
                    return $store;
                }
            }
        }, $list->toArray());

        return view('welcome', [
            'products' => $products,
            'stores' => $stores,
            'topStores' => $topStores,
        ]);
    }

    public function single($slug)
    {
        $product = Product::whereSlug($slug)->first();

        return view('single', [
            'product' => $product,
        ]);
    }
}
