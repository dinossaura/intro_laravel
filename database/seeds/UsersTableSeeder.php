<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //criar 40 usuários e para cada usuário criar um objeto store
        factory(\App\User::class, 40)->create()->each(function ($user) {
            //make retorna uma instânica do objeto store com os parâmetros passados na sua factory
            $user->store()->save(factory(\App\Store::class)->make());
        });
    }
}
